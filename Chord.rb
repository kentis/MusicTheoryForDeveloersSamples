class Chord
    include Enumerable
    attr :root
    attr :pattern

    def initialize(root, pattern = Chord.major())
        @root = root
        @pattern = pattern
    end

    def each
        Skala.new(root, Skala.major()).each_with_index {|n, i|
        if @pattern.include?(i)
            yield n
        elsif @pattern.any? {|n| n[0] == i.to_s && n[1] == "b"}
            yield n.prev()
        elsif @pattern.any? {|n| n[0] == i.to_s && n[1] == "#"}
            yield n.next()
        end
        }
    end

    def self.major
        [0,2,4]
    end

    def self.minor
        [0,"2b",4]
    end
end



use_synth :piano
a = My::Note.new('a', 440.0)

c = a.next("c")
f = a.next("f")
g = a.next("g")

crd = Chord.new(c)


crd = Chord.new(c)
crd.play()
sleep(1)

crd = Chord.new(f, Chord.minor())
crd.each { |n|
puts n.name
play play hz_to_midi( n.pitch), sustain: 1.5, release: 0.5
}
sleep(1)

crd = Chord.new(g)
crd.play()
sleep(1)

