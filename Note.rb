class Note
  attr :name
  attr :pitch
  attr :notes
  
  def initialize(name, pitch, notes = ['a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#'])
    @name = name
    @pitch = pitch
    @notes = notes
  end
  
  def succ
    succ_name = @notes[(@notes.index(@name)+1) % @notes.length]
    succ_pitch = @pitch * 2**(1/@notes.length.to_f)
    Note.new(succ_name, succ_pitch, @notes)
  end
  
  def prev
    prev_name = @notes[(@notes.index(@name)-1) % @notes.length]
    prev_pitch = @pitch / 2**(1/@notes.length.to_f)
    Note.new(prev_name, prev_pitch, @notes)
  end

  def <=>(other)
    raise TypeError unless other.kind_of? Note
    @pitch.round(5) <=> other.pitch.round(5)
  end

  def next(name)
    n = self.succ
    while(n.name != name) do
        n = n.succ
      end
      return n
    end
  end
end


use_synth :piano
a = My::Note.new('a', 440.0)

play hz_to_midi( a.pitch), release: 0.5
sleep(0.5)

c = a.next("c")
play hz_to_midi( c.pitch), release: 0.5