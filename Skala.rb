class Skala
    include Enumerable

    attr :pattern
    attr :root
    attr :index
    attr :current
    attr :octaves

    def initialize(root, pattern, octaves=1)
        @root = root
        @pattern = pattern
        @octaves = octaves
        @current = root
        @index = 0
    end

    def each
        s = Skala.new(@root, @pattern)
        max = @root
        @octaves.times{ max = max.next(max.name) }
        while((s.current <=> max) <= 0)
        yield s.current
        s.step()
        end
    end

    def step
        @pattern[@index % @pattern.length].times { |i|
        @current = @current.succ()
        }
        @index = @index + 1
    end

    def self.major
        [2,2,1,2,2,2,1]
    end
end

use_synth :piano
a = My::Note.new('a', 440.0)

myscale = Skala.new(a, Skala.major())
myscale.each do |n|
  puts "name: #{n.name}, pitch: #{n.pitch}"
  play hz_to_midi( n.pitch), release: 0.5
  sleep 0.5
  myscale.step()
end